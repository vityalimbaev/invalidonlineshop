
CREATE TABLE user (
	id INT (1) NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(255) NOT NULL,
	second_name VARCHAR(255) NOT NULL,
    third_name  VARCHAR(255),
    login VARCHAR(255)  NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE admin (
	id INT(1) NOT NULL AUTO_INCREMENT,
    id_user INT(1) NOT NULL,
    position VARCHAR(255) NOT NULL,
	PRIMARY KEY(id),
    FOREIGN KEY (id_user) REFERENCES user (id)
);

CREATE TABLE client (
	id INT (1) NOT NULL AUTO_INCREMENT,
    id_user INT(1) NOT NULL,
    address VARCHAR(255),
    email VARCHAR(255) NOT NULL,
    phone VARCHAR(255),
    deposit INT (1),
    FOREIGN KEY (id_user) REFERENCES user (id),
	PRIMARY KEY(id)
);

CREATE TABLE category(
	id INT (1) NOT NULL AUTO_INCREMENT,
	name_group VARCHAR(255),
	id_parent INT(1),
    PRIMARY KEY(id),
    FOREIGN KEY (id_parent) REFERENCES category (id)
);

CREATE TABLE product (
	id INT(1) NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    price INT (1),
    count INT (1),
    manufacturer VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE products_group(
	id_product INT (1) NOT NULL,
    id_category INT (1) NOT NULL,
    FOREIGN KEY (id_product) REFERENCES product (id),
    FOREIGN KEY (id_category) REFERENCES category (id)
);

CREATE TABLE basket(
	id_product INT(1),
    id_client INT (1),
    count INT (1),
    FOREIGN KEY (id_product) REFERENCES product (id),
    FOREIGN KEY (id_client) REFERENCES client (id)
);

