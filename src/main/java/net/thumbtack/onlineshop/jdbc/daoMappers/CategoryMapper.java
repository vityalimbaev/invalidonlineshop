package net.thumbtack.onlineshop.jdbc.daoMappers;

import net.thumbtack.onlineshop.jdbc.model.Product;
import org.springframework.jdbc.core.RowMapper;
import net.thumbtack.onlineshop.jdbc.model.Category;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CategoryMapper implements RowMapper<Category> {

    public Category mapRow(ResultSet resultSet, int i) throws SQLException {
        Category category = new Category();
        category.setId(resultSet.getInt("id"));
        category.setName(resultSet.getString("name"));
        category.setParentID(resultSet.getInt("id_parent"));

        while (resultSet.next()) {
            Product product = new Product();
            product.setId(resultSet.getInt("product_id"));
            product.setPrice(resultSet.getInt("price"));
            product.setCount(resultSet.getInt("count"));
            product.setManufacturer(resultSet.getString("manufacturer"));
            product.setName(resultSet.getString("name"));
            category.addProduct(product);
        }

        return category;
    }
}
