package net.thumbtack.onlineshop.jdbc.daoMappers;

import net.thumbtack.onlineshop.jdbc.model.Basket;
import net.thumbtack.onlineshop.jdbc.model.Client;
import net.thumbtack.onlineshop.jdbc.model.Product;
import net.thumbtack.onlineshop.jdbc.model.User;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Component
public class ClientMapper implements RowMapper<Client> {

    public Client mapRow(ResultSet resultSet, int i) throws SQLException {
        Map<Integer, Client> clients = new HashMap<Integer, Client>();
        int idClient = 0;
        while (resultSet.next()) {
            Client client = null;
            idClient = resultSet.getInt("client_id");

            if (clients.containsKey(idClient)) {
                client = clients.get(idClient);
            } else {
                client = getClient(resultSet);
                User user = getUser(resultSet);
                client.setUser(user);
                clients.put(idClient, client);
            }
            client.getBasket().addProduct(getProduct(resultSet), resultSet.getInt("choice_count"));
        }
        return clients.get(idClient);
    }

    private Client getClient(ResultSet resultSet) throws SQLException {
        Client client = new Client();
        client.setId(resultSet.getInt("id"));
        client.setAddress(resultSet.getString("address"));
        client.setDeposit(resultSet.getInt("deposit"));
        client.setEmail(resultSet.getString("email"));
        client.setPhone(resultSet.getString("phone"));
        return client;
    }

    private User getUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("user_id"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setSecondName(resultSet.getString("second_name"));
        user.setThirdName(resultSet.getString("third_name"));
        user.setLogin(resultSet.getString("login"));
        user.setPassword(resultSet.getString("password"));
        return user;
    }

    private Product getProduct(ResultSet resultSet) throws SQLException {
        Product product = new Product();
        product.setId(resultSet.getInt("product_id"));
        product.setName(resultSet.getString("name"));
        product.setPrice(resultSet.getInt("price"));
        product.setCount(resultSet.getInt("count"));
        product.setManufacturer(resultSet.getString("manufacturer"));
        return product;
    }
}
