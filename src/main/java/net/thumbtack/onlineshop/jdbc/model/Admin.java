package net.thumbtack.onlineshop.jdbc.model;

public class Admin {

    private int id;
    private String position;
    private User user;

    public Admin() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "id=" + id +
                ", user=" + user +
                '}';
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setUser(User user) {
        if(this.user == null) {
            this.user = user;
        }
    }
}
