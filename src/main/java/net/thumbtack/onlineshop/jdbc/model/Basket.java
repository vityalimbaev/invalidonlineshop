package net.thumbtack.onlineshop.jdbc.model;

import java.util.HashMap;
import java.util.Map;

public class Basket {
    private Map<Product, Integer> mapProducts;
    private int id_client;

    public Basket() {
        super();
        mapProducts = new HashMap<Product, Integer>();
    }

    public int getId_client() {
        return id_client;
    }

    public void setId_client(int id_client) {
        this.id_client = id_client;
    }


    public void addProduct(Product product, int count){
        mapProducts.put(product, count);
    }

    public Map<Product, Integer> getMapProducts() {
        return mapProducts;
    }

    public void setMapProducts(Map<Product, Integer> mapProducts) {
        this.mapProducts = mapProducts;
    }
}
