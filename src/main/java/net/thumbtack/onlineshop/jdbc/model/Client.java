package net.thumbtack.onlineshop.jdbc.model;

import com.sun.istack.internal.NotNull;

public class Client {
    private int id;
    private int deposit;
    private Basket basket;
    private User user;
    private String address;
    private String phone;
    private String email;

    public Client() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public Basket getBasket() {
        return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }

    public void setUser(User user) {
        if(this.user == null) {
            this.user = user;
        }
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", deposit=" + deposit +
                ", basket=" + basket +
                ", user=" + user +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
