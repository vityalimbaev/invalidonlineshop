package net.thumbtack.onlineshop.jdbc.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Category {
    private int id;
    private int parentID;
    private List<Product> listProduct;
    private String name;

    public Category() {
        listProduct = new ArrayList<Product>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addProduct(Product product){
        if(product == null) throw new IllegalArgumentException();
        if(!listProduct.contains(product)) listProduct.add(product);
    }

    public List<Product> getListProduct() {
        return listProduct;
    }

    public void setListProduct(List<Product> listProduct) {
        this.listProduct = listProduct;
    }

    public int getParentID() {
        return parentID;
    }

    public void setParentID(int parentID) {
        this.parentID = parentID;
    }

}
