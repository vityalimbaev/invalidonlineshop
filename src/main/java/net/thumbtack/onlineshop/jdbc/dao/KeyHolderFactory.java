package net.thumbtack.onlineshop.jdbc.dao;

import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public class KeyHolderFactory {
    private KeyHolder keyHolder;

    public KeyHolderFactory (){
        keyHolder = new GeneratedKeyHolder();
    }

    public KeyHolder getKeyHolder() {
        return keyHolder;
    }

    public int getIntKey(){
        try {
            return keyHolder.getKey().intValue();
        }catch (NullPointerException e) {
            return 0;
        }
    }
}
