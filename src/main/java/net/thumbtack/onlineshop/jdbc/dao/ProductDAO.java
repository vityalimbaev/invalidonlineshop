package net.thumbtack.onlineshop.jdbc.dao;

import net.thumbtack.onlineshop.jdbc.daoMappers.ProductMapper;
import net.thumbtack.onlineshop.jdbc.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

@Service
public class ProductDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private ProductMapper productMapper;

    private final String SQL_INSERT = "INSERT INTO products (name,count,price,manufacturer) VALUES (:name, :count, :price, :manufacturer)";
    public final String SQL_UPDATE = "UPDATE products SET name = :name, price = :price, count = :count, manufacturer = :manufacturer" +
            "WHERE id = :id";
    public final String SQL_DELETE = "DELETE * FROM products WHERE id = ?";
    public final String SQL_SELECT_ALL = "SELECT * FROM products";
    public final String SQL_SELECT_BY_ID = SQL_SELECT_ALL + " WHERE id = ?";
    public final String SQL_SELECT_BY_NAME = SQL_SELECT_ALL + "WHERE name = ?";
    public final String SQL_SELECT_BY_MANUFACTURER = SQL_SELECT_ALL + "WHERE manufacture = ?";

    @Autowired
    public ProductDAO(DataSource dataSource, ProductMapper productMapper) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.productMapper = productMapper;
    }

    public Product insertProduct(Product product) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("name", product.getName())
                .addValue("price", product.getPrice())
                .addValue("count", product.getCount())
                .addValue("manufacturer", product.getManufacturer());
        namedParameterJdbcTemplate.update(SQL_INSERT, sqlParameterSource, keyHolder);
        product.setId(keyHolder.getKey().intValue());
        return product;
    }

    public Product updateProduct(Product product) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", product.getId())
                .addValue("name", product.getName())
                .addValue("price", product.getPrice())
                .addValue("count", product.getCount())
                .addValue("manufacturer", product.getManufacturer());
        namedParameterJdbcTemplate.update(SQL_UPDATE, sqlParameterSource);
        return product;
    }

    public Product deleteProduct(Product product) {
        jdbcTemplate.update(SQL_DELETE, product.getId());
        return product;
    }

    public List<Product> selectAllProduct() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productMapper);
    }

    public Product selectProductByID(int id) {
        List<Product> list = jdbcTemplate.query(SQL_SELECT_BY_ID, new Object[]{id}, productMapper);
        for(Product product : list){
            if(product.getId() == id){
                return product;
            }
        }
        return null;
    }

    public Product selectProductByName(String name) {
        if (name == null) throw new IllegalArgumentException();

        List<Product> list = jdbcTemplate.query(SQL_SELECT_BY_NAME, new Object[]{name}, productMapper);
        for(Product product : list){
            if(product.getName().equals( name)){
                return product;
            }
        }
        return null;
    }

    public List<Product> selectAllProductByManufacturer(String manufacturer) {
        if (manufacturer == null) throw new IllegalArgumentException();
        return jdbcTemplate.query(SQL_SELECT_BY_MANUFACTURER, new Object[]{manufacturer}, productMapper);
    }
}
