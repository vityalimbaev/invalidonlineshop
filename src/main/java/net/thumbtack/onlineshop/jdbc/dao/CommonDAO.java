package net.thumbtack.onlineshop.jdbc.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

@Service
public class CommonDAO {
    private JdbcTemplate jdbcTemplate;

    public CommonDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void clear() {
        jdbcTemplate.execute("DROP DATABASE IF EXISTS online_shop");

    }
}
