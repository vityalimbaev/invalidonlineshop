package net.thumbtack.onlineshop.jdbc.dao;

import net.thumbtack.onlineshop.jdbc.daoMappers.CategoryMapper;
import net.thumbtack.onlineshop.jdbc.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

@Service
public class CategoryDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private CategoryMapper categoryMapper;

    private final String SQL_INSERT = "INSERT INTO groups (name, id_parent) VALUES (:name, :parentID)";
    public final String SQL_SELECT_ALL = "SELECT * FROM groups";
    public final String SQL_UPDATE = "UPDATE groups SET name = :name, id_parent = parentID WHERE id = :id";
    public final String SQL_SELECT_BY_ID = SQL_SELECT_ALL + " WHERE groups WHERE id = ? ";
    public final String SQL_SELECT_BY_NAME = SQL_SELECT_ALL + " WHERE groups WHERE name = ?";

    @Autowired
    public CategoryDAO(DataSource dataSource, CategoryMapper categoryMapper) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.categoryMapper = categoryMapper;
    }

    public Category insertCategory(Category category) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("name", category.getName())
                .addValue("parentID", category.getParentID());
        namedParameterJdbcTemplate.update(SQL_INSERT, sqlParameterSource, keyHolder);
        category.setId(keyHolder.getKey().intValue());
        return category;
    }

    public Category updateCategory(Category category) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("name", category.getName())
                .addValue("parentID", category.getParentID())
                .addValue("id", category.getId());
        namedParameterJdbcTemplate.update(SQL_UPDATE, sqlParameterSource);
        return category;
    }

    public List<Category> selectAllCategory() {
        return jdbcTemplate.query(SQL_SELECT_ALL, categoryMapper);
    }


    public Category selectCategoryById(int id) {
        List<Category> list = jdbcTemplate.query(SQL_SELECT_BY_ID, new Object[]{id}, categoryMapper);
        for (Category category : list) {
            if (category.getId() == id) {
                return category;
            }
        }
        return null;
    }

    public Category selectCategoryByName(String name) {
        if (name == null) throw new IllegalArgumentException();
        List<Category> list = jdbcTemplate.query(SQL_SELECT_BY_NAME, new Object[]{name}, categoryMapper);
        for (Category category : list) {
            if (category.getName() == name) {
                return category;
            }
        }
        return null;
    }
}
