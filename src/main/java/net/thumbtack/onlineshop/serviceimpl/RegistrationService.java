package net.thumbtack.onlineshop.serviceimpl;

import net.thumbtack.onlineshop.jdbc.dao.AdminDAO;
import net.thumbtack.onlineshop.jdbc.dao.ClientDAO;
import net.thumbtack.onlineshop.jdbc.dao.UserDAO;
import net.thumbtack.onlineshop.jdbc.model.Admin;
import net.thumbtack.onlineshop.jdbc.model.Client;
import net.thumbtack.onlineshop.jdbc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegistrationService {
    private UserDAO userDAO;
    private ClientDAO clientDAO;
    private AdminDAO adminDAO;

    @Autowired
    public RegistrationService(UserDAO userDAO, ClientDAO clientDAO, AdminDAO adminDAO) {
        this.userDAO = userDAO;
        this.clientDAO = clientDAO;
        this.adminDAO = adminDAO;
    }

    public Admin registrationAdmin(User user) {
        return null;
    }

    public Client registrationClient(User user) {
        return null;
    }

    public void removeAdmin(Admin admin) {

    }

    public void removeClient(Client client) {

    }

    public List<Admin> getAllAdmin() {
        return null;
    }

    public List<Client> getAllClient() {
        return null;
    }
}
