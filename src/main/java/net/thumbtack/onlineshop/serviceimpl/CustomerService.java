package net.thumbtack.onlineshop.serviceimpl;

import net.thumbtack.onlineshop.jdbc.dao.ClientDAO;
import net.thumbtack.onlineshop.jdbc.dao.ProductDAO;
import net.thumbtack.onlineshop.jdbc.model.Client;
import net.thumbtack.onlineshop.jdbc.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CustomerService {
    private ClientDAO clientDAO;
    private ProductDAO productDAO;

    @Autowired
    public CustomerService(ClientDAO clientDAO, ProductDAO productDAO) {
        this.clientDAO = clientDAO;
        this.productDAO = productDAO;
    }


    public Client addCashToDeposit(int idSession) {
        return null;
    }

    public Client getDeposit(int idSession) {
        return null;
    }

    public String buyProduct(int idSession, int count) {
        return null;
    }

    public List<String> addToBasket(int idSession, Product product, int count) {
        return null;
    }

    public String removeProductFromBasket(int idSession) {
        return null;
    }

    public List<String> changeCountProductInBasket(int idSession, Product product, int count) {
        return null;
    }

    public List<String> getProductsFromBasket(int idSession) {
        return null;
    }

    public Map<Product, String> buyFromBasket(List<Product> products) {
        return null;
    }
}
