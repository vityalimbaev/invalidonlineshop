package net.thumbtack.onlineshop.serviceimpl;
import net.thumbtack.onlineshop.jdbc.dao.AdminDAO;
import net.thumbtack.onlineshop.jdbc.dao.ClientDAO;
import net.thumbtack.onlineshop.jdbc.model.Admin;
import net.thumbtack.onlineshop.jdbc.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActiveUserService {

    private ClientDAO clientDAO;
    private AdminDAO adminDAO;

    @Autowired
    public ActiveUserService(ClientDAO clientDAO, AdminDAO adminDAO) {
        this.clientDAO = clientDAO;
        this.adminDAO = adminDAO;
    }

    public Admin getMyAdminInformation(int idSession) {
        return null;
    }

    public Client getMyClientInformation(int idSession) {
        return null;
    }

    public Admin updateMayAdminInformation(int idSession) {
        return null;
    }

    public Client updateMayClientInformation(int idSession) {
        return null;
    }
}
