package net.thumbtack.onlineshop.serviceimpl;

import net.thumbtack.onlineshop.jdbc.dao.AdminDAO;
import net.thumbtack.onlineshop.jdbc.dao.CategoryDAO;
import net.thumbtack.onlineshop.jdbc.dao.ProductDAO;
import net.thumbtack.onlineshop.jdbc.model.Category;
import net.thumbtack.onlineshop.jdbc.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {
    private CategoryDAO categoryDAO;
    private ProductDAO productDAO;
    private AdminDAO adminDAO;

    @Autowired
    public AdminService(CategoryDAO categoryDAO, ProductDAO productDAO, AdminDAO adminDAO) {
        this.categoryDAO = categoryDAO;
        this.productDAO = productDAO;
        this.adminDAO = adminDAO;
    }

    public String addCategory(Category category, int idSession) {
        return null;
    }

    public Category getCategory(int idSession) {
        return null;
    }

    public Category editCategory(int idSession) {
        return null;
    }

    public String removeCategory(int idSession) {
        return null;
    }

    public List<Category> getAllCategory(int idSession) {
        return null;
    }

    public Product addProduct(int idSession, List<Category> categories) {
        return null;
    }

    public Product editProduct(Product product, List<Category> categories) {
        return null;
    }

    public String removeProduct(int idSession) {
        return null;
    }

    public Product getProduct(int idSession) {
        return null;
    }

    public List<Product> getListProducts(List<Category> categories, String order) {
        return null;
    }

    public String getStatement(int idSession) {
        return null;
    }

    public String getServerSettings(int idSession) {
        return null;
    }

    public String clearDataBase() {
        return null;
    }
}
