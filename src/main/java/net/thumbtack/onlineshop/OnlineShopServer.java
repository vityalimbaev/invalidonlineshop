package net.thumbtack.onlineshop;

import net.thumbtack.onlineshop.config.SpringJDBCConfig;

import net.thumbtack.onlineshop.serviceimpl.*;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
public class OnlineShopServer {
    private static ActiveUserService activeUserService;
    private static AdminService adminService;
    private static CustomerService customerService;
    private static LogInAndOutService logInAndOutService;
    private static RegistrationService registrationService;

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringJDBCConfig.class);
        context.register(ActiveUserService.class);
        context.register(AdminService.class);
        context.register(CustomerService.class);
        context.register(LogInAndOutService.class);
        context.register(RegistrationService.class);
    }
}
