package net.thumbtack.onlineshop.config;

import net.thumbtack.onlineshop.jdbc.dao.*;
import net.thumbtack.onlineshop.jdbc.daoMappers.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@ComponentScan
@Configuration
public class SpringJDBCConfig {

    @Bean(name = "datSource")
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/online_shop?useUnicode=yes&characterEncoding=UTF8&useSSL=false&serverTimezone=Asia/Omsk");
        dataSource.setUsername("test");
        dataSource.setPassword("test");

        return dataSource;
    }

}
