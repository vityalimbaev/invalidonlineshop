package net.thumbtack.onlineshop.jdbcTest;

import net.thumbtack.onlineshop.jdbc.dao.UserDAO;
import net.thumbtack.onlineshop.jdbc.daoMappers.UserMapper;
import net.thumbtack.onlineshop.jdbc.model.User;
import org.junit.Test;
import org.mockito.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class UserDaoTest extends SetUpAndDataFactoryForTest {

    @InjectMocks
    UserDAO userDAO;
    @Mock
    DataSource dataSourceMock;
    @Mock
    UserMapper userMapperMock;
    @Mock
    NamedParameterJdbcTemplate namedParameterJdbcTemplateMock ;
    @Mock
    JdbcTemplate jdbcTemplateMock;
    @Mock
    KeyHolder keyHolderMock;

   /* @Test
    public void testInsertUserDao(){
        User user = new User();
        user.setId(1);
        user.setLogin("login");
        user.setPassword("password");
        user.setFirstName("name1");
        user.setSecondName("name2");
        user.setThirdName("name3");

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("name1", user.getFirstName())
                .addValue("name2", user.getSecondName())
                .addValue("name3", user.getThirdName())
                .addValue("login", user.getLogin())
                .addValue("password", user.getPassword());

        when(keyHolderFactory.getIntKey()).thenReturn(1);
        when(namedParameterJdbcTemplateMock.update(userDAO.SQL_INSERT, parameters, keyHolderFactory.getKeyHolder())).thenReturn(0);

        User userNew = userDAO.insertUser(user);
        assertEquals(user, userNew);

        verify(namedParameterJdbcTemplateMock, times(1))
                .update(userDAO.SQL_INSERT, parameters, keyHolderFactory.getKeyHolder());
    }*/

    @Test
    public void testUpdateUserDao(){
        User user1 = getUser(1);

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("name1", user1.getFirstName())
                .addValue("name2", user1.getSecondName())
                .addValue("name3", user1.getThirdName())
                .addValue("password", user1.getPassword())
                .addValue("id", user1.getId());

        when(namedParameterJdbcTemplateMock.update(userDAO.SQL_UPDATE, parameters)).thenReturn(0);

        User userNew = userDAO.updateUser(user1);
        assertEquals(user1, userNew);
    }

    @Test
    public void testDeleteUser() {
        User user1 = getUser(1);

        when(jdbcTemplateMock.update(userDAO.SQL_DELETE, 1)).thenReturn(0);

        User newUser = userDAO.deleteUser(user1);
        assertEquals(user1, newUser);

        verify(jdbcTemplateMock, times(1)).update(userDAO.SQL_DELETE, 1);
    }

    @Test
    public void testSelectAllUser() {
        User user1 = getUser(1);

        List<User> list = new ArrayList<>();
        list.add(user1);
        when(jdbcTemplateMock.query(userDAO.SQL_SELECT_ALL, userMapperMock)).thenReturn(list);

        List<User> newList = userDAO.selectAllUser();
        assertEquals(list, newList);
        verify(jdbcTemplateMock, times(1)).query(userDAO.SQL_SELECT_ALL, userMapperMock);
    }

    @Test
    public void testSelectUser() {

        List<User> users = getSomeUsers(2);

        when(jdbcTemplateMock.query(userDAO.SQL_SELECT_BY_ID,new Object[]{users.get(0).getId()}, userMapperMock)).thenReturn(users);
        User newUser1 = userDAO.selectUser(1);
        assertEquals(users.get(0), newUser1);

        when(jdbcTemplateMock.query(userDAO.SQL_SELECT_BY_ID,new Object[]{users.get(1).getId()}, userMapperMock)).thenReturn(users);
        User newUser2 = userDAO.selectUser(2);
        assertEquals(users.get(1), newUser2);

        verify(jdbcTemplateMock, times(1)).query(userDAO.SQL_SELECT_BY_ID,new Object[]{users.get(0).getId()}, userMapperMock);
        verify(jdbcTemplateMock, times(1)).query(userDAO.SQL_SELECT_BY_ID,new Object[]{users.get(0).getId()}, userMapperMock);
    }

    @Test
    public void testSelectUserByLogin() {

        List<User> users = getSomeUsers(2);

        when(jdbcTemplateMock.query(userDAO.SQL_SELECT_BY_LOGGER,new Object[]{"login1"}, userMapperMock)).thenReturn(users);
        User newUser1 = userDAO.selectUserByLogin("login1");
        assertEquals(users.get(0), newUser1);

        when(jdbcTemplateMock.query(userDAO.SQL_SELECT_BY_LOGGER,new Object[]{"login2"}, userMapperMock)).thenReturn(users);
        User newUser2 = userDAO.selectUserByLogin("login2");
        assertEquals(users.get(1), newUser2);

        verify(jdbcTemplateMock, times(1)).query(userDAO.SQL_SELECT_BY_LOGGER,new Object[]{"login1"}, userMapperMock);
        verify(jdbcTemplateMock, times(1)).query(userDAO.SQL_SELECT_BY_LOGGER,new Object[]{"login2"}, userMapperMock);
    }
}
