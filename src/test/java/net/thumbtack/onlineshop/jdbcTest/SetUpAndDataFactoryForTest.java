package net.thumbtack.onlineshop.jdbcTest;

import net.thumbtack.onlineshop.jdbc.model.*;
import org.junit.Before;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;


public class SetUpAndDataFactoryForTest {

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    public User getUser(int userId){
        User user = new User();
        user.setId(userId);
        user.setLogin("login"+userId);
        user.setPassword("password");
        user.setFirstName("name1");
        user.setSecondName("name2");
        user.setThirdName("name3");
        return user;
    }

    public Admin getAdmin(int userID, int adminId){
        Admin admin = new Admin();
        admin.setUser(getUser(userID));
        admin.setPosition("position");
        admin.setId(adminId);
        return admin;
    }

    public Client getClient(int clientID, int userId){
        Client client = new Client();
        client.setUser(getUser(userId));
        client.setBasket(new Basket());
        client.setPhone("123456");
        client.setEmail("email" + clientID +"@gmail.com");
        client.setAddress("address1");
        client.setDeposit(500000);
        client.setId(clientID);
        return client;
    }

    public Product getProduct(int productID, int manufacturerID){
        Product product = new Product();
        product.setId(productID);
        product.setName("name"+productID);
        product.setManufacturer("manufacturer"+manufacturerID);
        product.setCount(3);
        product.setPrice(250);
        return product;
    }

    public Category getCategory(int categoryID, int countProducts){
        Category category = new Category();
        category.setId(categoryID);
        category.setName("name"+categoryID);

        return category;
    }

    public List<Admin> getSomeAdmins(int count){
        List<Admin> listAdmin = new ArrayList<>();
        for(int i = 1; i <= count; i++){
            listAdmin.add(getAdmin(i,i));
        }
        return listAdmin;
    }

    public List<Client> getSomeClients(int count){
        List<Client> listClient = new ArrayList<>();
        for(int i = 1; i <= count; i++){
            listClient.add(getClient(i,i));
        }
        return listClient;
    }

    public List<Product> getSomeProducts(int count){
        List<Product> listProduct = new ArrayList<>();
        for(int i = 1; i <= count; i++){
            listProduct.add(getProduct(i,i));
        }
        return listProduct;
    }
    public List<Category> getSomeCategories(int count, int countProducts){
        List<Category> listCategories = new ArrayList<>();
        for(int i = 1; i <= count; i++){
            listCategories.add(getCategory(i,countProducts));
        }
        return listCategories;
    }
    public List<User> getSomeUsers(int count){
        List<User> listUser = new ArrayList<>();
        for(int i = 1; i <= count; i++){
            listUser.add(getUser(i));
        }
        return listUser;
    }

}
