package net.thumbtack.onlineshop.jdbcTest;

import net.thumbtack.onlineshop.jdbc.dao.CategoryDAO;
import net.thumbtack.onlineshop.jdbc.daoMappers.CategoryMapper;
import net.thumbtack.onlineshop.jdbc.model.Category;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CategoryDaoTest extends SetUpAndDataFactoryForTest {

    @InjectMocks
    CategoryDAO categoryDAO;
    @Mock
    DataSource dataSourceMock;
    @Mock
    CategoryMapper categoryMapperMock;
    @Mock
    NamedParameterJdbcTemplate namedParameterJdbcTemplateMock ;
    @Mock
    JdbcTemplate jdbcTemplateMock;
    @Mock
    SqlParameterSource sqlParameterSourceMock;


    @Test
    public void testUpdateCategory() {

        Category category = getCategory(1,1);

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("id", category.getId())
                .addValue("name", category.getName())
                .addValue("parent_id", category.getParentID());

        when(namedParameterJdbcTemplateMock.update(categoryDAO.SQL_UPDATE, parameters)).thenReturn(0);

        Category categoryNew = categoryDAO.updateCategory(category);
        assertEquals(category, categoryNew);
    }

    @Test
    public void testSelectAllCategory() {

        List<Category> list = getSomeCategories(1,1);
        when(jdbcTemplateMock.query(categoryDAO.SQL_SELECT_ALL, categoryMapperMock)).thenReturn(list);

        List<Category> listNew = categoryDAO.selectAllCategory();
        assertEquals(list,listNew);
        verify(jdbcTemplateMock, times(1) ).query(categoryDAO.SQL_SELECT_ALL, categoryMapperMock);
    }

    @Test
    public void selectCategoryById() {
        List<Category> categories = getSomeCategories(1,1);

        when(jdbcTemplateMock.query(categoryDAO.SQL_SELECT_BY_ID, new Object[]{categories.get(0).getId()}, categoryMapperMock)).thenReturn(categories);

        Category categoryNew = categoryDAO.selectCategoryById(categories.get(0).getId());
        assertEquals(categories.get(0),categoryNew);
        verify(jdbcTemplateMock, times(1) ).query(categoryDAO.SQL_SELECT_BY_ID,new Object[]{categories.get(0).getId()}, categoryMapperMock);
    }

    @Test
    public void testSelectCategoryByName() {
        List<Category> categories = getSomeCategories(1,1);

        when(jdbcTemplateMock.query(categoryDAO.SQL_SELECT_BY_NAME, new Object[]{categories.get(0).getName()}, categoryMapperMock)).thenReturn(categories);

        Category categoryNew = categoryDAO.selectCategoryByName(categories.get(0).getName());
        assertEquals(categories.get(0),categoryNew);

        verify(jdbcTemplateMock, times(1) ).query(categoryDAO.SQL_SELECT_BY_NAME,
                new Object[]{categories.get(0).getName()}, categoryMapperMock);
    }
}
