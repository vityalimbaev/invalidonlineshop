package net.thumbtack.onlineshop.jdbcTest;

import net.thumbtack.onlineshop.jdbc.dao.ProductDAO;
import net.thumbtack.onlineshop.jdbc.daoMappers.ProductMapper;
import net.thumbtack.onlineshop.jdbc.model.Product;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ProductDaoTest extends SetUpAndDataFactoryForTest {
    @InjectMocks
    ProductDAO productDAO;
    @Mock
    DataSource dataSourceMock;
    @Mock
    ProductMapper productMapperMock;
    @Mock
    NamedParameterJdbcTemplate namedParameterJdbcTemplateMock ;
    @Mock
    JdbcTemplate jdbcTemplateMock;
    @Mock
    SqlParameterSource sqlParameterSourceMock;

    @Test
    public void testUpdateProductDao(){
        Product product = getProduct(1,1);

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("id", product.getId())
                .addValue("name",product.getName())
                .addValue("price", product.getPrice())
                .addValue("count", product.getCount())
                .addValue("manufacturer", product.getManufacturer());

        when(namedParameterJdbcTemplateMock.update(productDAO.SQL_UPDATE, parameters)).thenReturn(0);

        Product productNew = productDAO.updateProduct(product);
        assertEquals(product, productNew);
    }

    @Test
    public void testDeleteProduct() {
        Product product = getProduct(1,1);

        when(jdbcTemplateMock.update(productDAO.SQL_DELETE,1)).thenReturn(0);

        Product newProduct = productDAO.deleteProduct(product);
        assertEquals(product,newProduct);

        verify(jdbcTemplateMock,times(1)).update(productDAO.SQL_DELETE,1);

    }

    @Test
    public void testSelectAllProduct() {
        Product product = getProduct(1,1);

        List<Product> list = new ArrayList<>();
        list.add(product);
        when(jdbcTemplateMock.query(productDAO.SQL_SELECT_ALL, productMapperMock)).thenReturn(list);

        List<Product> newList = productDAO.selectAllProduct();
        assertEquals(list, newList);
        verify(jdbcTemplateMock, times(1)).query(productDAO.SQL_SELECT_ALL, productMapperMock);
    }

    @Test
    public void testSelectProduct() {
        List<Product> products = getSomeProducts(2);

        when(jdbcTemplateMock.query(productDAO.SQL_SELECT_BY_ID,new Object[]{products.get(0).getId()}, productMapperMock)).thenReturn(products);
        Product newProduct1 = productDAO.selectProductByID(1);
        assertEquals(products.get(0), newProduct1);

        when(jdbcTemplateMock.query(productDAO.SQL_SELECT_BY_ID,new Object[]{products.get(1).getId()}, productMapperMock)).thenReturn(products);
        Product newProduct2 = productDAO.selectProductByID(2);
        assertEquals(products.get(1), newProduct2);

        verify(jdbcTemplateMock, times(1)).query(productDAO.SQL_SELECT_BY_ID,new Object[]{products.get(0).getId()}, productMapperMock);
        verify(jdbcTemplateMock, times(1)).query(productDAO.SQL_SELECT_BY_ID,new Object[]{products.get(1).getId()}, productMapperMock);
    }

    @Test
    public void testSelectProductByName() {

        List<Product> products = getSomeProducts(2);

        when(jdbcTemplateMock.query(productDAO.SQL_SELECT_BY_NAME, new Object[]{products.get(0).getName()}, productMapperMock)).thenReturn(products);
        Product newProduct1 = productDAO.selectProductByName("name1");
        assertEquals(products.get(0), newProduct1);

        when(jdbcTemplateMock.query(productDAO.SQL_SELECT_BY_NAME, new Object[]{products.get(1).getName()}, productMapperMock)).thenReturn(products);
        Product newProduct2 = productDAO.selectProductByName("name2");
        assertEquals(products.get(1), newProduct2);

        verify(jdbcTemplateMock, times(1)).query(productDAO.SQL_SELECT_BY_NAME, new Object[]{products.get(0).getName()}, productMapperMock);
        verify(jdbcTemplateMock, times(1)).query(productDAO.SQL_SELECT_BY_NAME, new Object[]{products.get(1).getName()}, productMapperMock);
    }

    @Test
    public void testSelectAllProductByManufacturer() {

        List<Product> products = getSomeProducts(2);
        products.add(getProduct(3,1));

        List<Product> request = new ArrayList<>();
        request.add(getProduct(1,1));
        request.add(getProduct(3,1));

        when(jdbcTemplateMock.query(productDAO.SQL_SELECT_BY_MANUFACTURER, new Object[]{products.get(0).getManufacturer()}, productMapperMock)).thenReturn(request);
        List<Product> newList = productDAO.selectAllProductByManufacturer("manufacturer1");
        assertEquals(request, newList);
        assertEquals(2, newList.size());


        verify(jdbcTemplateMock, times(1)).query(productDAO.SQL_SELECT_BY_MANUFACTURER, new Object[]{products.get(0).getManufacturer()}, productMapperMock);
        verify(jdbcTemplateMock, times(1)).query(productDAO.SQL_SELECT_BY_MANUFACTURER, new Object[]{products.get(2).getManufacturer()}, productMapperMock);
    }
}
