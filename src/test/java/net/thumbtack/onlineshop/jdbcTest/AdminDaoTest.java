package net.thumbtack.onlineshop.jdbcTest;

import net.thumbtack.onlineshop.jdbc.dao.AdminDAO;
import net.thumbtack.onlineshop.jdbc.daoMappers.AdminMapper;
import net.thumbtack.onlineshop.jdbc.model.Admin;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class AdminDaoTest extends SetUpAndDataFactoryForTest {
    @InjectMocks
    protected AdminDAO adminDAO;


    @Mock
    DataSource dataSourceMock;
    @Mock
    AdminMapper adminMapperMock;
    @Mock
    NamedParameterJdbcTemplate namedParameterJdbcTemplateMock;
    @Mock
    JdbcTemplate jdbcTemplateMock;
    @Mock
    SqlParameterSource sqlParameterSourceMock;

    @Test
    public void testUpdateAdminDao() {
        Admin admin = getAdmin(1, 1);

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("idUser", admin.getUser().getId())
                .addValue("position", admin.getPosition());

        when(namedParameterJdbcTemplateMock.update(adminDAO.SQL_UPDATE, parameters)).thenReturn(0);

        Admin admin1New = adminDAO.updateAdmin(admin);
        assertEquals(admin, admin1New);
    }

    @Test
    public void testDeleteAdmin() {
        Admin admin = getAdmin(1, 1);

        when(jdbcTemplateMock.update(adminDAO.SQL_DELETE, 1)).thenReturn(0);

        Admin newAdmin = adminDAO.deleteAdmin(admin);
        assertEquals(admin, newAdmin);

        verify(jdbcTemplateMock, times(1)).update(adminDAO.SQL_DELETE, 1);
    }

    @Test
    public void testSelectAllAdmin() {
        Admin admin = getAdmin(1, 1);

        List<Admin> list = new ArrayList<>();
        list.add(admin);
        when(jdbcTemplateMock.query(adminDAO.SQL_SELECT_ALL, adminMapperMock)).thenReturn(list);

        List<Admin> newList = adminDAO.selectAllAdmins();
        assertEquals(list, newList);
        verify(jdbcTemplateMock, times(1)).query(adminDAO.SQL_SELECT_ALL, adminMapperMock);
    }

    @Test
    public void testSelectAdmin() {

        List<Admin> admins = getSomeAdmins(2);

        when(jdbcTemplateMock.query(adminDAO.SQL_SELECT_BY_ID, new Object[]{admins.get(0).getId()}, adminMapperMock)).thenReturn(admins);
        Admin newAdmin1 = adminDAO.select(1);
        assertEquals(admins.get(0), newAdmin1);

        when(jdbcTemplateMock.query(adminDAO.SQL_SELECT_BY_ID, new Object[]{admins.get(1).getId()}, adminMapperMock)).thenReturn(admins);
        Admin newAdmin2 = adminDAO.select(2);
        assertEquals(admins.get(1), newAdmin2);

        verify(jdbcTemplateMock, times(1)).query(adminDAO.SQL_SELECT_BY_ID, new Object[]{admins.get(0).getId()}, adminMapperMock);
        verify(jdbcTemplateMock, times(1)).query(adminDAO.SQL_SELECT_BY_ID, new Object[]{admins.get(1).getId()}, adminMapperMock);
    }

    @Test
    public void testSelectAdminByLogin() {

        List<Admin> admins = getSomeAdmins(2);

        when(jdbcTemplateMock.query(adminDAO.SQL_SELECT_BY_LOGIN, new Object[]{admins.get(0).getUser().getLogin()}, adminMapperMock)).thenReturn(admins);
        Admin newAdmin1 = adminDAO.select(admins.get(0).getUser().getLogin());
        assertEquals(admins.get(0), newAdmin1);

        when(jdbcTemplateMock.query(adminDAO.SQL_SELECT_BY_LOGIN, new Object[]{admins.get(1).getUser().getLogin()}, adminMapperMock)).thenReturn(admins);
        Admin newAdmin2 = adminDAO.select(admins.get(1).getUser().getLogin());
        assertEquals(admins.get(1), newAdmin2);

        verify(jdbcTemplateMock, times(1)).query(adminDAO.SQL_SELECT_BY_LOGIN, new Object[]{admins.get(0).getUser().getLogin()}, adminMapperMock);
        verify(jdbcTemplateMock, times(1)).query(adminDAO.SQL_SELECT_BY_LOGIN, new Object[]{admins.get(1).getUser().getLogin()}, adminMapperMock);
    }
}
