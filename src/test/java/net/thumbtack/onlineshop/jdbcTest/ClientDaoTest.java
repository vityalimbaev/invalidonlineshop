package net.thumbtack.onlineshop.jdbcTest;

import net.thumbtack.onlineshop.jdbc.dao.ClientDAO;
import net.thumbtack.onlineshop.jdbc.daoMappers.ClientMapper;
import net.thumbtack.onlineshop.jdbc.model.Client;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ClientDaoTest extends SetUpAndDataFactoryForTest {
    @InjectMocks
    protected ClientDAO clientDAO;

    @Mock
    DataSource dataSourceMock;
    @Mock
    ClientMapper clientMapperMock;
    @Mock
    NamedParameterJdbcTemplate namedParameterJdbcTemplateMock ;
    @Mock
    JdbcTemplate jdbcTemplateMock;

    @Test
    public void testUpdateClientDao(){
        Client client = getClient(1,1);

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("idUser", client.getUser().getId())
                .addValue("position",client.getAddress())
                .addValue("address", client.getPhone())
                .addValue("phone", client.getPhone());

        when(namedParameterJdbcTemplateMock.update(clientDAO.SQL_UPDATE, parameters)).thenReturn(0);

        Client clientNew = clientDAO.updateClient(client);
        assertEquals(client,clientNew);
    }

    @Test
    public void testDeleteClient() {
        Client client = getClient(1,1);

        when(jdbcTemplateMock.update(clientDAO.SQL_DELETE,1)).thenReturn(0);

        Client newClient = clientDAO.deleteClient(client);
        assertEquals(client,newClient);

        verify(jdbcTemplateMock,times(1)).update(clientDAO.SQL_DELETE,1);
    }

    @Test
    public void testSelectAllClient() {

        List<Client> list = getSomeClients(2);

        when(jdbcTemplateMock.query(clientDAO.SQL_SELECT_ALL, clientMapperMock)).thenReturn(list);

        List<Client> newList = clientDAO.selectAllClient();
        assertEquals(list, newList);
        verify(jdbcTemplateMock, times(1)).query(clientDAO.SQL_SELECT_ALL, clientMapperMock);
    }

    @Test
    public void testSelectClient() {
        List<Client> clients = getSomeClients(2);

        when(jdbcTemplateMock.query(clientDAO.SQL_SELECT_BY_ID,new Object[]{clients.get(0).getId()}, clientMapperMock)).thenReturn(clients);
        Client newClient1 = clientDAO.selectClient(1);
        assertEquals(clients.get(0), newClient1);

        when(jdbcTemplateMock.query(clientDAO.SQL_SELECT_BY_ID,new Object[]{clients.get(1).getId()}, clientMapperMock)).thenReturn(clients);
        Client newClient2 = clientDAO.selectClient(2);
        assertEquals(clients.get(1), newClient2);

        verify(jdbcTemplateMock, times(1)).query(clientDAO.SQL_SELECT_BY_ID,new Object[]{clients.get(0).getId()}, clientMapperMock);
        verify(jdbcTemplateMock, times(1)).query(clientDAO.SQL_SELECT_BY_ID,new Object[]{clients.get(1).getId()}, clientMapperMock);
    }

    @Test
    public void testSelectUserByLogin() {

        List<Client> clients = getSomeClients(2);

        when(jdbcTemplateMock.query(clientDAO.SQL_SELECT_BY_LOGIN,new Object[]{clients.get(0).getUser().getLogin()}, clientMapperMock)).thenReturn(clients);
        Client newClient1 = clientDAO.selectClientByLogin("login1");
        assertEquals(clients.get(0), newClient1);

        when(jdbcTemplateMock.query(clientDAO.SQL_SELECT_BY_LOGIN,new Object[]{clients.get(1).getUser().getLogin()}, clientMapperMock)).thenReturn(clients);
        Client newClient2 = clientDAO.selectClientByLogin("login2");
        assertEquals(clients.get(1), newClient2);

        verify(jdbcTemplateMock, times(1)).query(clientDAO.SQL_SELECT_BY_LOGIN,new Object[]{clients.get(0).getUser().getLogin()}, clientMapperMock);
        verify(jdbcTemplateMock, times(1)).query(clientDAO.SQL_SELECT_BY_LOGIN,new Object[]{clients.get(1).getUser().getLogin()}, clientMapperMock);
    }
}
